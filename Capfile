set :application, 'bkt'
# uncomment the below line if you encounter error: `require': cannot load such file -- rvm/capistrano
# $:.unshift("#{ENV["HOME"]}/.rvm/lib")
require "rvm/capistrano"
set :rvm_type, :user
set :rvm_ruby_string, "2.1.2@#{application}"

require 'capistrano/ext/multistage'
require 'bundler/capistrano'
require 'capistrano_colors'

load 'deploy'

set :default_environment, {
                            'PATH' => "#{deploy_to}/bin:$PATH",
                            'GEM_HOME' => "#{deploy_to}/gems"
                        }

set :stages, %w(production)

set :stage, 'production'
set :branch, 'master'
set :domain, 'dev.fulllouis.com'

set :default_stage, 'production'

task :production do
  set :stage, 'production'
  set :branch, 'master'
  set :domain, 'dev.fulllouis.com'
end

# set :deploy_via, :remote_cache
set :scm, :git
set :repository, "git@bitbucket.org:anhlhv/bkt.git"
set :user, 'eric'
set(:deploy_to) { "/home/#{user}/#{application}_#{stage}" }
set :chmod755, 'app config db lib public'
set :use_sudo, false
role(:app) { domain }
role(:web) { domain }
role(:db, :primary => true) { domain }

namespace :bundle do
  task :install, :roles => [:app] do
    run "cd #{release_path} && bundle install --without development test deploy"
  end
end

namespace :rvm do
  task :trust_rvmrc do
    run "rvm rvmrc trust #{release_path}"
  end
end

namespace :deploy do
  task :restart, :roles => [:app]  do
    run "touch #{current_path}/tmp/restart.txt"
  end
  desc "Update the crontab file"
  task :update_crontab, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && bundle exec whenever --update-crontab #{application}"
  end


  task :reindex, :roles => :app, :except => { :no_release => true } do
    run "cd #{current_path} && bundle exec rake searchkick:reindex:all RAILS_ENV=production"
  end
end

namespace :assets do
  task :precompile, :roles => [:app] do
    run "source ~/.rvm/scripts/rvm"
    run "cd #{current_path} && bundle exec rake assets:precompile RAILS_ENV=#{stage}"
  end
end

after 'deploy:update_code', 'rvm:trust_rvmrc', 'deploy:create_symlink', 'assets:precompile'
after 'deploy', 'deploy:cleanup'

task :sync do
  on_rollback { delete delete '/tmp/dump' }
  dump_path = File.expand_path('dump')
  conf = YAML.load_file('config/mongoid.yml')
  prod_db = conf[stage]['sessions']['default']['database']
  dump_file = "#{application}-mongo-dump.tar.gz"
  run "cd /tmp && mongodump #{x=conf[stage]['username'] && "-u#{x}"} #{x=conf[stage]['password'] && "-p#{x}"} -d#{prod_db}"
  run "cd /tmp/dump && tar vcfz #{dump_file} #{prod_db}"

  %x[mkdir -p #{dump_path}]
  Dir.chdir(dump_path)
  p "====="
  get "/tmp/dump/#{dump_file}", dump_file
  dev_db = conf['development']['sessions']['default']['database']
  p "====="
  %x[rm -rf #{conf[stage]['database']} #{dev_db}]
  %x[tar vxfz #{dump_file}]
  %x[rm #{dump_file}]
  %x[mv #{prod_db} #{dev_db}]
  %x[cd .. && mongorestore --drop]
end

task :backup do
  on_rollback { delete delete '/tmp/dump' }
  dump_path = File.expand_path('dump')
  conf = YAML.load_file('config/mongoid.yml')
  prod_db = conf[stage]['sessions']['default']['database']
  file_name = Time.now.strftime('%y%m%d-%H%M')
  dump_file = "#{file_name}.tar.gz"
  run "cd /tmp && mongodump #{x=conf[stage]['username'] && "-u#{x}"} #{x=conf[stage]['password'] && "-p#{x}"} -d#{prod_db}"
  run "cd /tmp/dump && tar vcfz #{dump_file} #{prod_db}"

  backup_path = File.expand_path('backup')
  %x[mkdir -p #{backup_path}]
  Dir.chdir(backup_path)
  p "====="
  last_progress = 0
  get("/tmp/dump/#{dump_file}", dump_file, :via => :scp) do |channel, name, sent, total|
    progress = (sent.to_f/total.to_f).round(1)
    if progress > last_progress
      last_progress = progress
      p "========== Downloading: #{progress*100}% complete"
    end
  end
end

## Fast deploy & utilities

set(:deploy_update) { "cd #{current_path} && git pull origin master && bundle" }
set(:deploy_restart) { "cd #{current_path} && touch tmp/restart.txt" }
set(:deploy_precompile_assets) { "cd #{current_path} && bundle exec rake assets:precompile" }

CMDS = 'git commit -am "changes"; git pull; git push'
task :d do
  %x[#{CMDS}]
  run deploy_update
  run deploy_restart
end

task :dd do
  %x[#{CMDS}]
  run deploy_update
  run deploy_precompile_assets
  run deploy_restart
end

task :t do
  # run "tail -f /opt/nginx/logs/error.log"
  run "tail -f #{current_path}/log/production.log"
end

task :remake do
  run "cd #{current_path} && bundle exec rake db:remake RAILS_ENV=#{stage}"
end
