$(window).load(function() {
  $('#slider').nivoSlider({
      controlNav: false,
      prevText: "<i class='fa fa-chevron-left fa-2x'></i>",                 // Prev directionNav text
      nextText: "<i class='fa fa-chevron-right fa-2x'></i>",                 // Next directionNav text
  });
});

$(function() {
  $(".btn-search").on( "click", function() {
    if ( $(".input-keyword").val().length > 0)
    {
      window.location.replace("/search?q=" + $(".input-keyword").val());
    }
    else {
      $(".input-keyword").focus();
    }
  })
});
