module Api
  class Api::ServiceController < ActionController::Base

    def upload
      @picture = Picture.new
      @picture.file = params[:file]
      if @picture.save
        render json: { filelink: @picture.file.url }
      else
        render json: { error: @picture.errors }
      end
    end

  end
end
