class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :site_info

  private
  def site_info
    View.create(ip: request.ip.to_s) if request.try(:ip).present?
    @slides = Slide.all
    @menus = Category.where(ancestry: nil).asc(:position)
    @latest_articles = Article.desc(:created_at).limit(5)
    @homepage_article = Article.where(homepage: true).first
  end
end
