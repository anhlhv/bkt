class ArticleController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  def index
  end

  def show
    @article = Article.find params[:id]
  end
end
