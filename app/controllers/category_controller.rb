class CategoryController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  def index
  end

  def show
    @category = Category.find params[:id]
    @articles = @category.articles
  end
end
