class TagsController < ApplicationController
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  def index
  end

  def show
    @articles = Article.tagged_with(params[:id])
  end
end
