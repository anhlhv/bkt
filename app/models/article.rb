class Article
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  include Trackable
  include Mongoid::History::Trackable
  include Mongoid::Userstamp
  include Mongoid::Taggable
  include Mongoid::Slug
  include Mongoid::Search

  field :title
  slug :title
  field :description
  field :content
  field :enabled, type: Boolean, default: true
  field :homepage, type: Boolean, default: false
  field :tags

  search_in :title, :description, :content

  scope :published, ->{ where(enabled: true).desc(:created_at) }

  has_mongoid_attached_file :image,
                        :url  =>  "/pictures/:id/:style.:extension",
                        :path => ":rails_root/public/pictures/:id/:style.:extension",
                        :styles => { :thumb => '100x100#' }

  validates_attachment :image, :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

  belongs_to :category
  
  has_many :related_articles, :class_name => 'Article', :inverse_of => :related_article
  belongs_to :related_article, :class_name => 'Article', :inverse_of => :related_articles

  def tags_indexing
    Article.create!(:tags => tags) if tags_changed?
  end

  before_update :tags_indexing
  validates_presence_of :title, :content, :image

  def content_show
    content.html_safe
  end

  def slug
    _slugs.first
  end

  rails_admin do
    list do
      field :title
      field :category
    end
    show do
      field :title
      field :content_show
    end
    edit do
      field :title
      field :image, :paperclip
      field :description
      field :content, :text do
         partial 'form_redactor'
       end
      field :category
      field :enabled
      field :homepage
      field :tags
      field :related_articles
    end
  end
end
