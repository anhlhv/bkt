class Category
  include Mongoid::Document
  include Mongoid::Paperclip
  include Trackable
  include Mongoid::History::Trackable
  include Mongoid::Userstamp
  include Mongoid::Slug
  include Mongoid::Ancestry
  has_ancestry


  field :title
  slug :title
  field :position

  default_scope ->{ asc(:position) }

  # belongs_to :menu
  has_many :articles

  def slug
    _slugs.first
  end

  rails_admin do
    nestable_tree({
      position_field: :position,
      max_depth: 2,
      live_update: true
    })
    list do
      field :title
      # field :menu
    end
    show do
      field :title
      # field :menu
    end
    edit do
      field :title
      # field :menu
      field :articles do
        orderable true
      end
    end
  end
end
