class Picture
  include Mongoid::Document
  include Mongoid::Paperclip

  has_mongoid_attached_file :file,
                          :url  =>  "/pictures/:id/:style.:extension",
                          :path => ":rails_root/public/pictures/:id/:style.:extension",
                          :styles => { :thumb => '300x300#'}

  validates_attachment :file, :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }
end
