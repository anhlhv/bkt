class Setting
  include Mongoid::Document

  field :key
  field :value

  def self.get_value key_text
    Setting.find_by(key: key_text).try(:value).to_s.html_safe
  end

  rails_admin do
    list do
      field :key
      field :value
    end
    show do
      field :key
      field :value
    end
    edit do
      field :key
      field :value, :text
    end
  end
end
