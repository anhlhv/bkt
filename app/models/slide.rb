class Slide
  include Mongoid::Document
  include Mongoid::Paperclip
  include Trackable
  include Mongoid::History::Trackable
  include Mongoid::Userstamp

  field :url
  has_mongoid_attached_file :image,
                        :url  =>  "/pictures/:id/:style.:extension",
                        :path => ":rails_root/public/pictures/:id/:style.:extension",
                        :styles => { :thumb => '194x100#' }

  validates_attachment :image, :content_type => { :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"] }

  rails_admin do
    list do
      field :image
    end
    show do
      field :image
      field :url
    end
    edit do
      field :image, :paperclip
      field :url
    end
  end
end
