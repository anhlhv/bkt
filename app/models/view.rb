require 'chronic'
require 'chronic_duration'
class View
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include RailsAdminCharts

  field :ip

  rails_admin do
    list do
      field :ip
      field :created_at
    end
  end

  def self.graph_data since=4.weeks.ago
    data, xaxis = chars_data
    [
      {
          name: 'Views',
          data: data
      }
    ]
  end

  def self.xaxis
    data, xaxis = chars_data
    xaxis
  end

  def self.chars_data
    data = []
    xaxis = []

    [4,3,2,1,0].each do |index|
      lower_bound = Time.now - ChronicDuration::parse("#{index + 1} week")
      if index == 0
        upper_bound = Time.now
      else
        upper_bound =  Time.now - ChronicDuration::parse("#{index} week")
      end

      views = self.where(:created_at.gt => lower_bound, :created_at.lte => upper_bound)
      data <<  views.count
      xaxis << "#{lower_bound.strftime("%d/%m")} - #{upper_bound.strftime("%d/%m")}"
    end
    return data, xaxis
  end
end
