Mongoid::History.modifier_class_name = 'User'
Mongoid::History.tracker_class_name = :history_tracker
Mongoid::Userstamp.config do |c|
  c.user_reader = :current_user
  c.user_model = :user
end
