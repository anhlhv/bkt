RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0
  config.audit_with :mongoid_audit

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard  do
      except ['View']
    end                    # mandatory
    index                     # mandatory
    new do
      except ['View']
    end
    export do
      except ['View']
    end
    bulk_delete do
      except ['View']
    end
    show do
      except ['View']
    end
    edit do
      except ['View']
    end
    delete do
      except ['View']
    end
    show_in_app do
      except ['View']
    end

    ## With an audit adapter, you can add:
    history_index do
      except ['View']
    end
    history_show do
      except ['View']
    end
    nestable
    charts
  end
end
